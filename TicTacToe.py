import sys
import os

def clear():
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')

def change_turn(turn, no_of_players):
    turn = (turn + 1) % no_of_players
    return turn

def error(message, also_exit=False):
    print(message, file=sys.stderr)
    if also_exit:
        exit(-1)

class Board:
    def __init__(self, x_dimension):
        self.board = []
        self.counter = 0
        self.empty_board_character = ' '
        self.x_dimension = x_dimension
        self.y_dimension = x_dimension
        self.board_length = self.x_dimension ** 2
        self.playable = True
        self.clear_board()
        del x_dimension
    
    def clear_board(self):
        self.board = [self.empty_board_character for _ in range(self.board_length)]
        
    def help_board(self):
        board_row = ""
        for i in range(self.board_length):
            if i != 0 and i % self.x_dimension == 0:
                board_row += ' |' + '\n'
            board_row += ' | ' + str(i + 1)
        board_row += ' |'
        print(board_row)
        print("\n")
        
    def display_board(self):
        board_row = ""
        for i in range(self.board_length):
            if i != 0 and i % self.x_dimension == 0:
                board_row += ' |' + '\n'
            board_row += ' | ' + str(self.board[i])
        board_row += ' |'
        print(board_row)
        print("\n")
    
    def put(self, index, character):
        if index > len(self.board):
            return False
        elif self.board[index] == self.empty_board_character:
            self.board[index] = character
            self.counter += 1
            self.check_playable()
            return True
        else:
            return False
        
    def check_playable(self):
        if self.empty_board_character in self.board:
            self.playable = True
            return True
        else:
            self.playable = False
            return False
    
    def get_board_data(self):
        row = []
        rows = []
        col = []
        cols = []
        diag = []
        diags = []
        row_index = 0
        col_index = 0
        diag_index = 0
        while row_index < self.board_length:
            row.append(self.board[row_index])
            row_index += 1
            if not row_index == 0 and row_index % self.x_dimension == 0:
                rows.append(row)
                row = []
                
        while diag_index < len(rows):
            for row in rows:
                diag.append(row[diag_index])
                if diag_index == self.x_dimension - 1:
                    diags.append(diag)
                    diag = []
                diag_index += 1
            diag_index = 0
            for row in rows[::-1]:
                diag.append(row[diag_index])
                if diag_index == self.x_dimension - 1:
                    diags.append(diag)
                    diag = []
                diag_index += 1
                
        while col_index < len(rows):
            for row in rows:
                col.append(row[col_index])
                if len(col) == self.x_dimension:
                    cols.append(col)
                    col = []
            col_index += 1       
        return rows + cols + diags
        
class Player:
    def __init__(self, board_object, symbol, name = "Anonymous"):
        self.board = board_object
        self.name = name
        self.symbol = symbol
        self.won = False
        self.ideal_condition = [self.symbol for _ in range(self.board.x_dimension)]
        del board_object
        
    def put(self, index):
        index -= 1
        if self.board.put(index, self.symbol):
            self.check_win()
            return True
        else:
            return False
        
    def check_win(self):
        if self.ideal_condition in board.get_board_data():
            self.won = True
            return True
        else:
            return False

correct_board_value = False
while not correct_board_value:
    width = input("Enter the board width[Default: 3]: ")
    try:
        width = int(width)
    except:
        width = 3
        print("Default width of 3 provided")
        correct_board_value = True
        continue
    else:
        if width < 3:
            error("\nInvalid board width")
            continue
        else:
            correct_board_value = True
            continue

board = Board(width)
options = {
 "help": board.help_board,
 "display": board.display_board
}
option_list = [option for option in options]
game_is_on = True

players = []
player_info_accepted = False
while not player_info_accepted:
    for player_id in range(2):
        players.append(Player(board_object = board, name=input(f"\nEnter the name for player {player_id + 1}: "), symbol=input(f"Enter the symbol for player {player_id + 1}: ")))
    if (players[0].name != players[1].name and players[0].symbol != players[1].symbol) and (players[0].symbol != "" or players[1].symbol != "") and (players[0].symbol != board.empty_board_character and players[1].symbol != board.empty_board_character):
        player_info_accepted = True
        clear()
        print("\n")
    else:
        error("\n Invalid or duplicate input provided")
        player_info_accepted = False
        players = []
        
game_is_on = True
turn = 0
while game_is_on:    
    input_value_accepted = False
    while not input_value_accepted:
        board.display_board()
        prompt = input(f"{players[turn].name}[Symbol: {players[turn].symbol}]: ")
        if prompt in option_list:
            options[prompt]()
            continue
        else:
            try:
                prompt = int(prompt)
            except:
                error("\n Must be an integer number, number without decimal point\n")
                continue
            else:
                if prompt <= 0 or prompt > board.board_length:
                    error(f"\n Input value must be greater than 0 and not exceed {board.board_length}\n")
                else:
                    input_value_accepted = True
        has_put = players[turn].put(prompt)
        clear()
        print("\n")
        if players[turn].won:
            clear()
            print("\n")
            board.display_board()
            print(f"{players[turn].name} using '{players[turn].symbol}' won")
            game_is_on = False
        elif not board.playable:
            clear()
            print("\n")
            board.display_board()
            print("It was a draw")
            game_is_on = False
            continue
        else:
            if has_put:
                turn = change_turn(turn, 2)
            else:
                print("Already exists")
                